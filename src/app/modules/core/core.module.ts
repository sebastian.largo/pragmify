import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { TracksComponent } from './pages/tracks/tracks.component';
import { CoreComponent } from './core.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LibraryComponent } from './pages/library/library.component';
import { FavoritesComponent } from './pages/favorites/favorites.component';
import { SearchComponent } from './pages/search/search.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TrackInterceptor } from './../../interceptors/track.interceptor';

@NgModule({
  declarations: [HomeComponent, TracksComponent, CoreComponent, LibraryComponent, FavoritesComponent, SearchComponent],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: TrackInterceptor, multi: true }],
  imports: [CommonModule, CoreRoutingModule, SharedModule],
})

export class CoreModule { }
