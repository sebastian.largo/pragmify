import { Component, OnInit } from '@angular/core';
import { Tracks } from 'src/app/interfaces/track.interface';
import { TracksService } from 'src/app/services/tracks.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  favTracks!: Tracks;

  constructor(private trackServ: TracksService) { }

  ngOnInit(): void {
    this.trackServ.getUserFavTracks().subscribe(data => {
      this.favTracks = data
    })
  }


}
