import { Component, OnInit } from '@angular/core';
import { SpotifyInterface } from 'src/app/interfaces/track.interface';
import { TracksService } from 'src/app/services/tracks.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  trackSearched: string = "";
  spotifyInfo?: SpotifyInterface;

  constructor(
    private trackServ: TracksService
  ) { }

  ngOnInit(): void {
  }

  getSearchArtists(query: string) {
    this.trackSearched = query;
    this.trackServ.search(query, ['artist', 'track', 'playlist', 'album', 'show', 'episode']).subscribe(data => {
      this.spotifyInfo = data;
    });
  }

}
