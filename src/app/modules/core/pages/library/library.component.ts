import { Component, OnInit } from '@angular/core';
import { SpotifyCategory } from 'src/app/interfaces/category.interface';
import { TracksService } from 'src/app/services/tracks.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {

  constructor(private trackServ: TracksService) { }

  categories!: SpotifyCategory;

  ngOnInit(): void {
    this.trackServ.getRecommendations().subscribe(data => {
      this.categories = data
    })
  }

}
