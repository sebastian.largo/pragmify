import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { TracksService } from 'src/app/services/tracks.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  constructor(private authServ: AuthService, private trackServ: TracksService) { }


  ngOnInit(): void {
    this.trackServ.accessToken.subscribe()
  }

}
