import { Component, OnInit } from '@angular/core';
import { SpotifyInterface } from 'src/app/interfaces/track.interface';
import { TracksService } from 'src/app/services/tracks.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.scss']
})
export class TracksComponent implements OnInit {

  trackSearched: string = "";
  spotifyInfo?: any;
  id: string | null = "";

  constructor(
    private trackServ: TracksService,
    private _Activatedroute: ActivatedRoute
  ) {
    this._Activatedroute.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
  }

  ngOnInit(): void {
    this.getSearchArtist(this.id != null ? this.id : "")
  }

  getSearchArtist(query: string) {
    this.trackSearched = query;
    this.trackServ.getAlbumTracks(query).subscribe(data => {
      this.spotifyInfo = data;
    });
  }

}
