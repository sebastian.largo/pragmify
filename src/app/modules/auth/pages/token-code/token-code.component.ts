import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { TracksService } from 'src/app/services/tracks.service';
// import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-token-code',
  templateUrl: './token-code.component.html',
  styleUrls: ['./token-code.component.scss'],
})
export class TokenCodeComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authServ: AuthService,
    private spotifyService: TracksService
  ) { }

  ngOnInit(): void {
    this.createAccessCode();
  }

  createAccessCode() {
    let accessToken: any;
    if (!this.route.snapshot.queryParams) return;
    const token = this.route.snapshot.queryParams['code'];
    this.authServ.getAccessToken(token).subscribe(result => {
      localStorage.setItem("AccessToken", result.access_token)
      this.spotifyService.accessToken.next(result.access_token);
    });
    this.authServ.setLogin(true)
    // this.store.dispatch(authActions.getToken({ token }));
    this.router.navigate(['/music/home']);
  }
}
