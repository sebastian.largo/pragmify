import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor() { }

  ngOnInit(): void { }

  goToCodeURL(): void {
    const LOGIN_URI: string = environment.LOGIN_URI;
    const client_id: string = environment.CLIENT_ID;
    const response_type: string = 'code';
    const scope: string = 'user-read-private user-read-email user-library-read user-read-recently-played';
    const redirect_uri: string = environment.CALL_BACK_URI;
    const show_dialog: boolean = true
    let codeURL: string = `${LOGIN_URI}?client_id=${client_id}&show_dialog=${show_dialog}&scope=${scope}&response_type=${response_type}&redirect_uri=${redirect_uri}`;
    location.replace(`${codeURL}`);
  }

}
