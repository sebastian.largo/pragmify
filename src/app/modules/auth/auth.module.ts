import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { AuthComponent } from './auth.component';
import { TokenCodeComponent } from './pages/token-code/token-code.component';

@NgModule({
  declarations: [LoginComponent, AuthComponent, TokenCodeComponent],
  imports: [CommonModule, AuthRoutingModule],
})
export class AuthModule {}
