import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Buffer } from 'buffer';
import { environment } from 'src/environments/environment';
// import { URLSearchParams } from 'url';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  isLogged: boolean = false;

  constructor(private http: HttpClient) {
    this.isLogged = localStorage.getItem("AccessToken") ? true : false;
  }

  setLogin(state: boolean) {
    this.isLogged = state;
  }

  getAccessToken(code: string): Observable<any> {
    const LOGIN_USER_URI: string = environment.LOGIN_USER_URI;
    const headers: HttpHeaders = new HttpHeaders({
      // 'Authorization': `Basic ${encoded}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    })
    const body: string = `code=${code}&grant_type=${'authorization_code'}&scopes=${'user-library-read'}&redirect_uri=${environment.CALL_BACK_URI}&client_id=${environment.CLIENT_ID}&client_secret=${environment.CLIENT_SECRET}`;

    return this.http
      .post(LOGIN_USER_URI, body, {
        headers: headers
      })
  }

  createEnconded64() {
    let newAuth = '';
    newAuth =
      'Basic ' +
      Buffer.from(
        environment.CLIENT_ID + ':' + environment.CLIENT_SECRET,
        'utf8'
      ).toString('base64');
    return newAuth;
  }
}
