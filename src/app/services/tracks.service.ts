import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { SpotifyInterface } from '../interfaces/track.interface';
import { AuthService } from '../modules/auth/services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class TracksService {

  public accessToken: BehaviorSubject<any> = new BehaviorSubject("");

  newAccessToken: string = ""


  constructor(private http: HttpClient) {
    this.accessToken.subscribe(data => {
      this.newAccessToken = data
    })
    localStorage.getItem("AccessToken") && (this.accessToken.next(localStorage.getItem("AccessToken")));
  }

  getTrack(query: string): Observable<any> {
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.newAccessToken}`,
    })
    return this.http.get<any>(`https://api.spotify.com/v1/tracks/${query}`, { headers: headers })
  }
  getAlbumTracks(query: string): Observable<any> {
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.newAccessToken}`,
    })
    return this.http.get<any>(`https://api.spotify.com/v1/artists/${query}/top-tracks?&market=ES`, { headers: headers })
  }

  getRecommendations(): Observable<any> {
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.newAccessToken}`,
    })
    return this.http.get<any>(`https://api.spotify.com/v1/browse/categories`, { headers: headers })
  }
  getUserFavTracks(): Observable<any> {
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.newAccessToken}`,
    })
    return this.http.get<any>(`https://api.spotify.com/v1/me/tracks`, { headers: headers })
  }

  search(query: string, type: string[]): Observable<SpotifyInterface> {
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.newAccessToken}`,
    })
    return this.http.get<SpotifyInterface>(`https://api.spotify.com/v1/search?type=${type}&include_external=audio&q=${query}`, { headers: headers })
  }


}
