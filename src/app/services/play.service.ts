import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayService {

  constructor() { }

  audio = new Audio();
  isPlaying: BehaviorSubject<boolean> = new BehaviorSubject(false);
  nameSong: BehaviorSubject<string> = new BehaviorSubject('');
  nameArtist: BehaviorSubject<string> = new BehaviorSubject('');

  setSongInfo(song: string, artist: string) {
    this.nameSong.next(song)
    this.nameArtist.next(artist)
  }

  setAudio(newSrc: string) {
    this.audio.src = newSrc
    this.audio.load()
  }

  play() {
    this.audio.play()
    this.isPlaying.next(true);
  }
  pause() {
    this.audio.pause()
    this.isPlaying.next(false);
  }


}
