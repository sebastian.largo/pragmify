import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

interface InfoCard {
  img?: string;
  type: string;
  name: string;
  genres: string[],
  id: string
}

@Component({
  selector: 'app-music-card',
  templateUrl: './music-card.component.html',
  styleUrls: ['./music-card.component.scss']
})
export class MusicCardComponent implements OnInit {

  @Input() item!: InfoCard;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToMoreInfo(type: string, id: string) {
    console.log(type);

    switch (type) {
      case 'artist':
        this.router.navigate(['/music/tracks'], { queryParams: { id: id } });
        break;
      // case 'track':
      //   this.router.navigate(['/music/tracks'], { queryParams: { id: id } });
      //   break;
      // case 'album':
      //   this.router.navigate(['/music/tracks'], { queryParams: { id: id } });
      //   break
      default:
        this.router.navigate(['/music/home']);
        break;
    }
  }

}
