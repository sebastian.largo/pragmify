import { Component, OnInit } from '@angular/core';
import { PlayService } from 'src/app/services/play.service';

@Component({
  selector: 'app-play-bar',
  templateUrl: './play-bar.component.html',
  styleUrls: ['./play-bar.component.scss']
})
export class PlayBarComponent implements OnInit {

  isPlaying: boolean = false;
  constructor(private playServ: PlayService) { }


  ngOnInit(): void {
    this.playServ.isPlaying.subscribe(data => {
      this.isPlaying = data
    })
  }


  play() {
    this.playServ.play()
  }
  pause() {
    this.playServ.pause()
  }

}
