import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormArray
} from "@angular/forms";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  @Output() searched = new EventEmitter<string>();

  searchForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) {
    this.searchForm = this.fb.group({
      searchControl: ['', []]
    });
  }

  ngOnInit(): void {
  }

  sendSearch() {
    const track = this.searchForm.get('searchControl')!.value;
    this.searched.emit(track);
  }

}
