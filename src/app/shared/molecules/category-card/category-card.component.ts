import { Component, Input, OnInit } from '@angular/core';
import { Item } from 'src/app/interfaces/category.interface';

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss']
})
export class CategoryCardComponent implements OnInit {

  @Input() item!: Item;
  @Input() index: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
