import { Component, Input, OnInit } from '@angular/core';
import { PlayService } from './../../../services/play.service';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.scss']
})

export class TrackListComponent implements OnInit {

  @Input() index!: number;
  @Input() track!: any;

  constructor(private playServ: PlayService) { }

  ngOnInit(): void {
  }

  playSong(src: string) {
    this.playServ.setSongInfo(this.track.name, this.track.artists[0] && this.track.artists[0].name)
    this.playServ.setAudio(src);
    this.playServ.play()
  }

}
