import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LargeMusicCardComponent } from './large-music-card.component';

describe('LargeMusicCardComponent', () => {
  let component: LargeMusicCardComponent;
  let fixture: ComponentFixture<LargeMusicCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LargeMusicCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LargeMusicCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
