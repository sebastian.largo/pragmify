import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicCardComponent } from './molecules/music-card/music-card.component';
import { FyTitleComponent } from './atoms/fy-title/fy-title.component';
import { LargeMusicCardComponent } from './molecules/large-music-card/large-music-card.component';
import { MainTitleComponent } from './atoms/main-title/main-title.component';
import { SideBarComponent } from './organisms/side-bar/side-bar.component';
import { SideNavComponent } from './templates/side-nav/side-nav.component';
import { NavBarComponent } from './organisms/nav-bar/nav-bar.component';
import { FooterComponent } from './organisms/footer/footer.component';
import { MusicListComponent } from './organisms/music-list/music-list.component';
import { MusicPopularComponent } from './organisms/music-popular/music-popular.component';
import { RouterModule } from '@angular/router';
import { PlayBarComponent } from './molecules/play-bar/play-bar.component';
import { SearchBarComponent } from './molecules/search-bar/search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { TrackListComponent } from './molecules/track-list/track-list.component';
import { MusicTrackListComponent } from './organisms/music-track-list/music-track-list.component';
import { CategoryCardComponent } from './molecules/category-card/category-card.component';



@NgModule({
  declarations: [
    MusicCardComponent,
    FyTitleComponent,
    LargeMusicCardComponent,
    MainTitleComponent,
    SideBarComponent,
    SideNavComponent,
    NavBarComponent,
    FooterComponent,
    MusicListComponent,
    MusicPopularComponent,
    PlayBarComponent,
    SearchBarComponent,
    TrackListComponent,
    MusicTrackListComponent,
    CategoryCardComponent,

  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    MusicCardComponent,
    FyTitleComponent,
    LargeMusicCardComponent,
    MainTitleComponent,
    SideBarComponent,
    SideNavComponent,
    NavBarComponent,
    FooterComponent,
    MusicListComponent,
    MusicPopularComponent,
    PlayBarComponent,
    SearchBarComponent,
    TrackListComponent,
    MusicTrackListComponent,
    CategoryCardComponent
  ]
})
export class SharedModule { }
