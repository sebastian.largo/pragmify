import { Component, Input, OnInit } from '@angular/core';
import { TracksItem } from 'src/app/interfaces/track.interface';

@Component({
  selector: 'app-music-track-list',
  templateUrl: './music-track-list.component.html',
  styleUrls: ['./music-track-list.component.scss']
})
export class MusicTrackListComponent implements OnInit {

  @Input() fyTitle: string = "";
  @Input() tracks!: any;

  constructor() { }

  ngOnInit(): void {
  }

}
