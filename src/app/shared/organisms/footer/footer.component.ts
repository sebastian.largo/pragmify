import { Component, OnInit } from '@angular/core';
import { PlayService } from 'src/app/services/play.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  nameSong: string = "";
  nameArtist: string = "";

  constructor(private playServ: PlayService) { }

  ngOnInit(): void {
    this.playServ.nameArtist.subscribe(data => {
      this.nameArtist = data;
    })
    this.playServ.nameSong.subscribe(data => {
      this.nameSong = data;
    })
  }

}
