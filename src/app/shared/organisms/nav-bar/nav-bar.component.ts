import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {

  @Input() scrolled: any;

  constructor(private authServ: AuthService, private router: Router) {
    /* TODO document why this constructor is empty */
  }
  ngOnInit(): void {
    /* TODO document why this method 'ngOnInit' is empty */
  }

  logOut() {
    this.authServ.setLogin(false);
    localStorage.removeItem("AccessToken");
    this.router.navigate(['auth/login'])
  }
}
