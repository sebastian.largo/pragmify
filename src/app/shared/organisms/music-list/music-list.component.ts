import { Component, Input, OnInit } from '@angular/core';
import { SpotifyInterface } from 'src/app/interfaces/track.interface';

@Component({
  selector: 'app-music-list',
  templateUrl: './music-list.component.html',
  styleUrls: ['./music-list.component.scss'],
})
export class MusicListComponent implements OnInit {

  @Input() fyTitle: string = "";
  @Input() musicTracks?: SpotifyInterface;

  constructor() { }

  ngOnInit(): void { }

  prueba() {
    console.log(this.musicTracks);

  }


}
