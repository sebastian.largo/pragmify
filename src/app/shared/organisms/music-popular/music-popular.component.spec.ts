import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicPopularComponent } from './music-popular.component';

describe('MusicPopularComponent', () => {
  let component: MusicPopularComponent;
  let fixture: ComponentFixture<MusicPopularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicPopularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MusicPopularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
