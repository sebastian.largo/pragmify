import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-music-popular',
  templateUrl: './music-popular.component.html',
  styleUrls: ['./music-popular.component.scss'],
})
export class MusicPopularComponent implements OnInit {


  currentTime: string = "¡Buena Mañana!";

  constructor() { }

  ngOnInit(): void {
    const now = new Date();
    const current = now.getHours();
    if (current > 18) {
      this.currentTime = '¡Buenas Noches!';
    } else if (current > 12) {
      this.currentTime = '¡Buenas Tardes!';
    }
  }
}
