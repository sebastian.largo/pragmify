import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent implements OnInit {

  @ViewChild("section") section!: ElementRef;

  scrolled: any = "";

  constructor() { }

  ngOnInit(): void { }

  setScroll(data: any) {
    this.scrolled = data;
  }
}
