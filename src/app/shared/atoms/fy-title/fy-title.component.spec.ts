import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FyTitleComponent } from './fy-title.component';

describe('FyTitleComponent', () => {
  let component: FyTitleComponent;
  let fixture: ComponentFixture<FyTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FyTitleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FyTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
