import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-fy-title',
  templateUrl: './fy-title.component.html',
  styleUrls: ['./fy-title.component.scss']
})
export class FyTitleComponent implements OnInit {

  @Input() title: string = 'titulo';

  constructor() { }

  ngOnInit(): void {
  }

}
