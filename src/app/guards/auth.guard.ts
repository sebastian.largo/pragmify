import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from '../modules/auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authServ: AuthService) {
  }

  canActivate() {
    let canAuth: boolean = true;
    canAuth = this.authServ.isLogged;
    if (canAuth) {
      this.router.navigate(['/music/home']);
    }
    return !canAuth;
  }

}
