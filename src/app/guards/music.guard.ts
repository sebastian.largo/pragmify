import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../modules/auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class MusicGuard implements CanActivate {

  constructor(private router: Router, private authServ: AuthService) {
  }
  canActivate(
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let canAuth: boolean = true;
    canAuth = this.authServ.isLogged;
    if (!canAuth) {
      this.router.navigate(['/auth/login']);
    }
    return canAuth;
  }

}
