import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MusicCardComponent } from './shared/molecules/music-card/music-card.component';
import { FyTitleComponent } from './shared/atoms/fy-title/fy-title.component';
import { LargeMusicCardComponent } from './shared/molecules/large-music-card/large-music-card.component';
import { MainTitleComponent } from './shared/atoms/main-title/main-title.component';
import { SideBarComponent } from './shared/organisms/side-bar/side-bar.component';
import { SideNavComponent } from './shared/templates/side-nav/side-nav.component';
import { NavBarComponent } from './shared/organisms/nav-bar/nav-bar.component';
import { FooterComponent } from './shared/organisms/footer/footer.component';
import { MusicListComponent } from './shared/organisms/music-list/music-list.component';
import { MusicPopularComponent } from './shared/organisms/music-popular/music-popular.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
