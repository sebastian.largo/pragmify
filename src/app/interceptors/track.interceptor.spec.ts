import { TestBed } from '@angular/core/testing';

import { TrackInterceptor } from './track.interceptor';

describe('TrackInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      TrackInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: TrackInterceptor = TestBed.inject(TrackInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
