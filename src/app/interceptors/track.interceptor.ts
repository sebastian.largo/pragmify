import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TracksService } from '../services/tracks.service';

@Injectable()
export class TrackInterceptor implements HttpInterceptor {

  constructor(private trackServ: TracksService) {
    // this.newAccessToken.subscribe(data => {
    //   this.newAccessToken = data
    // })
  }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const cloneReq = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${this.trackServ.newAccessToken}`)
    })
    return next.handle(cloneReq);
  }
}
