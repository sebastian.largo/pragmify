// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_BASE_URL: 'https://api.spotify.com/v1',
  LOGIN_USER_URI: 'https://accounts.spotify.com/api/token',
  CALL_BACK_URI: 'http://localhost:4200/auth/callback',
  LOGIN_URI: 'https://accounts.spotify.com/authorize',
  CLIENT_ID: 'b8cd29121e8c4a4ca3687ebbc171c1f2',
  CLIENT_SECRET: 'edc922d8c9454ffa8a2cff311806a2ce',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
